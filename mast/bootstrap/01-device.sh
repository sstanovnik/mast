#!/bin/sh

set -e
set -u

# to be safe
chmod u+x *.sh

echo "Installing prerequisites"
apt-get update
apt-get install -y python3 python3-venv python3-apt

echo "Ensuring the virtual environment exists"
if [ ! -d .venv ]; then
    python3 -m venv .venv
fi

# source is a bashism, dot is the sh one
# activate is not error-handling-friendly, temp disable
set +e
set +u
. .venv/bin/activate
set -e
set -u
pip3 install wheel
pip3 install --upgrade pip
pip3 install -r 01-requirements.txt

network_setup_file=$(find . -maxdepth 1 -iname '02-network-setup-*')

if [ "$(echo "$network_setup_file" | wc -l)" -ne 1 ]; then
    echo "There must be exactly one network setup file present."
    exit 2
fi

export PARENT_NAME="$(grep parent_name 03-vars.yml | sed -E 's/^parent_name: "(.*)"/\1/')"
echo "Running network setup, connecting to parent: $PARENT_NAME"
"./${network_setup_file}"


echo "Running ansible playbook"
ansible-playbook --extra-vars ansible_python_interpreter=/usr/bin/python3 --extra-vars @03-vars.yml 03-device.yml
