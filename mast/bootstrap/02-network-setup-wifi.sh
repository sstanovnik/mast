#!/bin/sh

set -e
set -u

echo "Finding WiFi interface..."
wifi_interface="$(ip l | grep -E 'wlp|wlan' | sed -E 's/[0-9]+: (.*): .*?/\1/')"
echo "Found interface: $wifi_interface"


# TODO: don't hardcode the password
echo >/etc/wpa_supplicant.conf <<EOF
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=SI

network={
    ssid="${PARENT_NAME}"
    psk="MastPasswordDifficultToBruteforce"
}
EOF

echo "Starting supplicant"
wpa_supplicant -B -i "$wifi_interface" -c /etc/wpa_supplicant.conf

echo "Triggering dhclient"
dhclient "$wifi_interface"
