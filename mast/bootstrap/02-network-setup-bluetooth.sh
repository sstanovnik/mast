#!/bin/sh

set -e
set -u

discover_device() {
    # IMPORTANT: if the device has already been discovered (is in bt-device --list),
    #            it will not appear in bt-adapter --discover

    # wait a minute for it to be discovered, hopefully, otherwise repeat, get mac from this
    truncate --size 0 /tmp/discover-output
    until grep -q -s "$PARENT_NAME" /tmp/discover-output; do
        echo "Starting discovery..."
        discover_output="$(bt-adapter --discover > /tmp/discover-output)" &
        discover_pid="$!"
        echo "Sleeping for 1 min"
        sleep 60
        echo "Sleep done, terminating $discover_pid"
        # SIGKILL
        kill -9 "$discover_pid"
        echo "Discovered devices:"
        cat /tmp/discover-output
    done
}

pair() {
    pnm="$1"
    echo "Connecting to device: $PARENT_NAME = $pnm"
    bt-device --connect "$pnm"
}

connect_pan() {
    pnm="$1"
    echo "Connecting to parent NAP network"
    bt-network --connect "$pnm" nap &

    # we know that the parent's internal IP is hardcoded everywhere
    # TODO: remove hardcoded parent PAN IP
    echo "Waiting for parent to become available"
    until ping -c 1 -W 1 10.223.1.1; do
        echo "   waiting..."
    done
    echo "    done!"

    echo "Removing default route through the PAN, we don't need it".
    ip route del default via 10.223.1.1 || true
}


echo "Checking if the network is up already"
if ping -c 1 -W 1 10.223.1.1; then
    echo "Network already up."
    exit 0
fi
echo "Network not up, continuing setup."

apt update -y
apt install -y bluetooth bluez-tools dbus
apt install -y pi-bluetooth || true
systemctl start bluetooth
systemctl enable bluetooth

echo "Trying to short-circuit if the parent device exists..."
bt-device --list > /tmp/existing-devices || true
echo "Existing devices:"
cat /tmp/existing-devices

if grep -q -s "$PARENT_NAME" /tmp/existing-devices; then
    echo "Device already discovered."
else
    echo "Device not discovered yet."
    discover_device
fi

echo "Getting parent device info"
bt-device --info "$PARENT_NAME" > /tmp/existing-device-info

echo "Getting parent device MAC"
parent_network_mac="$(grep --before-context 3 "Paired:" /tmp/existing-device-info | head -n 1 | sed -E 's/.*Address: (.*?)/\1/')"
echo "    got: $parent_network_mac"

echo "Checking if paired, pairing otherwise"
if [ "$(grep Paired /tmp/existing-device-info | sed -E 's/.*Paired: (.*?)/\1/')" != "1" ]; then
    pair "$parent_network_mac"
fi

connect_pan "$parent_network_mac"
