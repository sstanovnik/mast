domain = "mast"
datacenter = "dc"

{# only one node in a datacenter may be the bootstrap, and it's the root in our case #}
{# this is a legacy flag, but it works for us, unlike bootstrap_expect #}
{% if parent_ip is not defined or not parent_ip %}bootstrap = true{% endif %}

bind_addr = "{{ bind_interface_address }}"
advertise_addr = "{{ bind_interface_address }}"

# the root doesn't join anything, it is the root
retry_join = [{{ '"' + parent_ip + '"' if parent_ip is defined and parent_ip else '' }}]

recursors = ["{{ upstream_dns }}"]

data_dir = "/opt/consul"
encrypt = "Hcw5vFve4McQsCWCMeAcjQ=="

disable_update_check = true

performance {
  # a multiplier of 1 is "production", 5 is "low cost testing"
  raft_multiplier = 5
}

ports {
  dns = 53
}

server = true
ui = true
