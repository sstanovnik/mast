import os
import sqlite3
from typing import List
from typing import Optional

from flask import Flask, jsonify, g, request, send_file
import requests

# global here, which is very ugly, but fine for this poc
CONTROLLER_EXTERNAL_IP = os.getenv("CONTROLLER_EXTERNAL_IP")
assert CONTROLLER_EXTERNAL_IP is not None
assert CONTROLLER_EXTERNAL_IP != ""


class MastMachine:
    def __init__(self, machine_id: int, name: str, external_ip: str, parent_id: int, bootstrapped: bool,
                 downstream_connection_type: str):
        self.machine_id = machine_id
        self.name = name
        self.external_ip = external_ip
        self.parent_id = parent_id
        self.bootstrapped = bootstrapped
        self.downstream_connection_type = downstream_connection_type

        # populated later
        self.parent = None  # type: Optional[MastMachine]
        self.children = []  # type: List[MastMachine]
        self.depth = None  # type: int
        self.bfs_traversal_order_number = None  # type: int
        # 1-based, scoped to the parent
        self.child_index = None  # type: int
        self.subnet_ipv4 = None  # type: str
        self.subnet_ipv6 = None  # type: str
        self.subnet_ipv6_mask = None  # type: int
        self.vpn_ip = None  # type: str
        self.hierarchy_root = None  # type: MastMachine

    @classmethod
    def from_row(cls, row: sqlite3.Row):
        return cls(row["id"], row["name"], row["external_ip"], row["parent_id"], row["bootstrapped"] != 0,
                   row["downstream_connection_type"])

    @classmethod
    def create_hierarchy(cls, machines: List["MastMachine"]) -> List["MastMachine"]:
        # a seemingly very very bad and naive nlogn + n^2 implementation,
        # but let's not complicate our lives with a heap/dict
        # assures the root will be first, helps set the depth
        # another important invariant that this sets is that
        #
        # A MACHINE WILL ALWAYS HAVE ITS PARENT BEFORE IT IN THE SEQUENCE
        #
        # because there is no way to add a machine with a parent with a higher machine_id than itself,
        # assuming autoincrementing ids
        machines = sorted(machines, key=lambda m: m.machine_id if m.machine_id is not None else -1)

        # short-circuit for safety, ensures at least the root exists
        if not machines:
            return []

        for m in machines:
            if m.parent_id is None:
                m.parent = None
                m.depth = 0
                m.child_index = -1
                continue
            for potential_parent in machines:
                if m.parent_id == potential_parent.machine_id:
                    m.parent = potential_parent
                    m.depth = potential_parent.depth + 1
                    potential_parent.children.append(m)
                    m.child_index = len(potential_parent.children)  # makes this 1-based
                    break

        # this is also inefficient and could probably be merged with the above somehow, but still theta(n)
        machine_fifo = [machines[0]]
        bfs_index = 0
        while machine_fifo:
            m = machine_fifo.pop(0)
            m.bfs_traversal_order_number = bfs_index
            bfs_index += 1
            machine_fifo.extend(m.children)

        def get_child_subnet(parent_subnet_ipv6, child_index):
            parent_ipv6_parts = parent_subnet_ipv6.rstrip(":").split(":")

            # find out if we need to start a new IPv6 section or append to this one
            # if our parent is the root or there are no zeroes (available space) in the last segment
            if len(parent_ipv6_parts) == 4 or "0" not in parent_ipv6_parts[-1]:
                # start a new section
                child_ipv6_parts = list(parent_ipv6_parts)
                child_ipv6_parts.append("{}000".format(hex(child_index)[-1]))
            else:
                # modify the last section
                child_ipv6_parts = list(parent_ipv6_parts[:-1])
                # guaranteed not to fail
                last_section_first_zero = parent_ipv6_parts[-1].index("0")
                new_last_section = list(parent_ipv6_parts[-1])
                new_last_section[last_section_first_zero] = hex(child_index)[-1]
                new_last_section = ''.join(new_last_section)
                child_ipv6_parts.append(new_last_section)

            return ":".join(child_ipv6_parts) + "::"

        # these are values directly present in inventory_vars, but already computed here for clarity
        # also because vpn_ip and subnet_ipv6 depend on the parent's subnet_ipv6
        for m in machines:
            # TODO: this effectively limits the number of devices to 255
            m.subnet_ipv4 = "10.222.{}.0".format(m.bfs_traversal_order_number + 1)
            m.subnet_ipv6 = "fc00:0:0:8008::" if m.parent is None else get_child_subnet(m.parent.subnet_ipv6,
                                                                                        m.child_index)
            m.subnet_ipv6_mask = 64 + 4 * m.depth
            # this needs to be the IP from the parent's side
            # this is because, before complete provisioning is done, the child subnet doesn't exist
            # because the OpenVPN server hasn't been deployed yet
            if m.parent is not None:
                m.vpn_ip = m.parent.subnet_ipv6 + hex(int(0x1000 + m.child_index))[2:]
            else:
                m.vpn_ip = m.subnet_ipv6 + "1"
            m.hierarchy_root = machines[0]

            # basic sanity checks
            assert m.child_index < 16
            assert m.depth < 12

        return machines

    def inventory_vars(self) -> dict:
        return {
            # this is an override for initial bootstrap, where the VON network hasn't yet been set up
            "ansible_host": self.external_ip if self.parent is None and not self.bootstrapped else self.vpn_ip,
            "ansible_python_interpreter": "/usr/bin/python3",
            "subnet_ipv4": self.subnet_ipv4,
            "subnet_ipv6": self.subnet_ipv6,
            "subnet_ipv6_mask": self.subnet_ipv6_mask,
            "bind_interface_address": self.vpn_ip,
            "bfs_traversal_order_number": self.bfs_traversal_order_number,
            "parent": self.parent.name if self.parent is not None else None,
            "parent_ip": self.parent.vpn_ip if self.parent is not None else None,
            "root": self.hierarchy_root.name,
            "downstream_connection_type": self.downstream_connection_type
        }


def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect("mast.sqlite", detect_types=sqlite3.PARSE_DECLTYPES)  # type: sqlite3.Connection
        # need to do this for each connection
        g.db.execute("PRAGMA foreign_keys = ON;")
        g.db.row_factory = sqlite3.Row
    return g.db


def init_db():
    db = get_db()
    c = db.cursor()
    c.executescript("""
PRAGMA foreign_keys = ON;
CREATE TABLE IF NOT EXISTS device (    
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT UNIQUE NOT NULL,
    bootstrapped INTEGER NOT NULL,
    -- must match \1 in 02-network-setup-(.*?).sh
    downstream_connection_type TEXT NOT NULL,
    -- from the point of view of its children, populated when bootstrapping completes
    external_ip TEXT,
    parent_id INTEGER,
    FOREIGN KEY(parent_id) REFERENCES device(id)
)""")
    c.close()
    db.commit()


def create_app():
    new_app = Flask(__name__)

    with new_app.app_context():
        init_db()

        # create the root as soon as the application starts
        existing_machines = get_machines()
        if not existing_machines:
            create_machine("mast-root", CONTROLLER_EXTERNAL_IP, None, "eth")

        def close_db(*args):
            db = g.pop('db', None)
            if db is not None:
                db.close()

        new_app.teardown_appcontext(close_db)
    return new_app


def get_machines():
    c = get_db().cursor()  # type: sqlite3.Cursor
    c.execute("SELECT * FROM device;")
    rows = c.fetchall()
    machines = [MastMachine.from_row(row) for row in rows]
    c.close()
    return MastMachine.create_hierarchy(machines)


def create_machine(name: str, external_ip: Optional[str], parent_name: Optional[str], downstream_connection_type: str):
    db = get_db()
    c = db.cursor()  # type: sqlite3.Cursor

    # short-circuit if already created
    c.execute("SELECT id FROM device WHERE name = ?", (name,))
    existing = c.fetchall()
    if len(existing) == 1:
        return
    elif len(existing) > 1:
        raise Exception("More than one existing row filtered by an unique constraint, how?")

    if parent_name is not None:
        c.execute("SELECT id FROM device WHERE name = ?", (parent_name,))
        parent_id = c.fetchone()["id"]
    else:
        parent_id = None
    c.execute("INSERT INTO device (name, external_ip, parent_id, downstream_connection_type, bootstrapped) "
              "VALUES (?, ?, ?, ?, 0)", (name, external_ip, parent_id, downstream_connection_type))
    c.close()
    db.commit()


app = create_app()


@app.route("/", methods=["GET"])
def inventory():
    with app.app_context():
        machines = get_machines()
        return jsonify({
            "_meta": {
                "hostvars": {m.name: m.inventory_vars() for m in machines}
            },
            "all": {
                "children": [
                    "ungrouped"
                ]
            },
            "ungrouped": {
                "hosts": [m.name for m in machines]
            }
        })


@app.route("/device-bootstrap-done", methods=["POST"])
def device_bootstrap_done():
    print(request, flush=True)
    print(request.headers, flush=True)
    print(request.data, flush=True)
    print(request.get_json(), flush=True)
    with app.app_context():
        db = get_db()
        c = db.cursor()  # type: sqlite3.Cursor
        params = request.get_json()
        if "name" not in params:
            return "Missing name.", 400
        c.execute("UPDATE device SET bootstrapped = 1 WHERE name = ?", (params["name"],))
        c.close()
        db.commit()

    # trigger provisioning
    templates_response = requests.get("http://localhost:8052/api/v2/workflow_job_templates/", auth=("admin", "password"))
    templates_json = templates_response.json()
    template = next(t for t in templates_json["results"] if t["name"] == "complete-runtime")
    launch_response = requests.post("http://localhost:8052" + template["related"]["launch"], auth=("admin", "password"))
    print(launch_response, flush=True)
    return "OK"


@app.route("/device-downstream-network-done", methods=["POST"])
def device_downstream_network_done():
    print(request, flush=True)
    print(request.headers, flush=True)
    print(request.data, flush=True)
    print(request.get_json(), flush=True)
    with app.app_context():
        db = get_db()
        c = db.cursor()  # type: sqlite3.Cursor
        params = request.get_json()
        if "name" not in params:
            return "Missing name.", 400
        if "external_ip" not in params:
            return "Missing external IP.", 400
        c.execute("UPDATE device SET external_ip = ? WHERE name = ?", (params["external_ip"], params["name"],))
        c.close()
        db.commit()

    return "OK"


@app.route("/get-new-machine-bootstrap", methods=["GET"])
def get_new_machine_bootstrap():
    if "name" not in request.args:
        return "Missing name", 400
    if "parent" not in request.args:
        return "Missing parent name", 400
    if "connection_type" not in request.args:
        return "Missing connection type", 400

    name = request.args.get("name")
    parent_name = request.args.get("parent")
    connection_type = request.args.get("connection_type")  # this is what the client provides downstream

    with app.app_context():
        create_machine(name, None, parent_name, connection_type)
        machines = get_machines()
        root_machine = next(m for m in machines if m.parent_id is None)
        parent_machine = next(m for m in machines if m.name == parent_name)
        new_machine = next(m for m in machines if m.parent_id == parent_machine.machine_id)

        import subprocess
        import tempfile
        with tempfile.NamedTemporaryFile(suffix=".ini", delete=False) as inventory_file:
            inventory_contents = """
[root]
{} ansible_host={} ansible_ssh_common_args='-o StrictHostKeyChecking=no'

[parent]
{} ansible_host={} ansible_ssh_common_args='-o StrictHostKeyChecking=no'
            """.format(root_machine.name, root_machine.vpn_ip, parent_machine.name, parent_machine.vpn_ip)

            print("Inventory:", flush=True)
            print(inventory_contents, flush=True)
            inventory_file.write(inventory_contents.encode("utf-8"))
            inventory_file.flush()

            launch_args = [
                "ansible-playbook",
                "-e", "ansible_ssh_private_key_file=/root/.ssh/id_awx_control",
                # ca-gen
                "-e", "parent_name={}".format(parent_machine.name),
                "-e", "destination_dir={}".format("bundle-{}".format(new_machine.name)),
                "-e", "control_ssh_pubkey_file={}".format("/root/.ssh/id_awx_control.pub"),
                "-e", "ca_key_file={}".format("/etc/ssl/private/ca.key".format(root_machine.name)),
                "-e", "ca_cert_file={}".format("/etc/ssl/private/ca.crt".format(root_machine.name)),
                "-e", "zip_file_remote={}".format("bundle-{}.zip".format(new_machine.name)),
                "-e", "zip_file_local={}".format("bundle-{}.zip".format(new_machine.name)),
                "-e", "cert_common_name={}".format(new_machine.name),
                "-e", "cert_country_name={}".format("SI"),
                "-e", "cert_organization_name={}".format("mast"),
                "-e", "cert_subject_alt_name={}".format("DNS:{}".format(new_machine.name)),
                "-e", "cert_key_usage={}".format('["nonRepudiation","digitalSignature","keyAgreement"]'),
                "-e", "cert_extended_key_usage={}".format('["clientAuth"]'),
                "-e", "cert_basic_constraints={}".format('["CA:FALSE"]'),
                # parent setup
                "-e", "client_name={}".format(new_machine.name),  # also in makebundle
                "-e", "client_subnet_ipv6={}".format(new_machine.subnet_ipv6),
                "-e", "client_subnet_ipv6_mask={}".format(new_machine.subnet_ipv6_mask),
                "-e", "client_assigned_ipv6={}".format(new_machine.vpn_ip),
                "-e", "parent_subnet_ipv6_mask={}".format(parent_machine.subnet_ipv6_mask),
                "-e", "parent_ipv6={}".format(parent_machine.vpn_ip),
                # makebundle
                "-e", "parent_external_ip={}".format(parent_machine.external_ip),
                "-e", "bundle_directory={}".format("bundle-full-{}".format(new_machine.name)),
                "-e", "cert_bundle_file={}".format("bundle-{}.zip".format(new_machine.name)),
                "-e", "client_bundle_file={}".format("bundle-full-{}.zip".format(new_machine.name)),
                "-e", "bootstrap_files_directory={}".format("/root/bootstrap/"),
                "-e", "client_connection_type={}".format(parent_machine.downstream_connection_type),
                "-e", "root_ipv6={}".format(root_machine.vpn_ip),
                "-i", inventory_file.name,
                "02-make-client-bundle.yml"
            ]
            print("Launch args:", flush=True)
            print(launch_args, flush=True)
            print(" ".join(launch_args), flush=True)

            completed = subprocess.run(launch_args, check=False)

        print(completed.stdout, flush=True)
        print(completed.stderr, flush=True)
        if completed.returncode == 0:
            return send_file("bundle-full-{}.zip".format(new_machine.name), as_attachment=True)
        else:
            return "Internal error.", 500


if __name__ == '__main__':
    app.run("::", 8008)
