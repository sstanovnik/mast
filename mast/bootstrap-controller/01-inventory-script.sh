#!/bin/sh
# we ignore --list and --host and always return the same response
# this is because --list is first issued and, because we return _meta,
# no further --host calls are issue because it knows everything
curl -sL http://{{ mast_controller_ip }}:{{ mast_controller_port }}/
