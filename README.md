# mast, edge infrastructure management

_Part of my Master's thesis._

Create a cutting-edge edge infrastructure using Ansible, AWX, OpenVPN, GlusterFS and more!

## Usage

Prepare at least two machines.
They can be physical, virtual, LXD-based, whatever.
Only physical machines with WiFi/Bluetooth adapters support the downstream WiFi/Bluetooth connection modes.

In this example, both machines are LXD containers for ease of testing:

* `mast-root` is the root controller machine
* `mast-1` is the child machine

The two interfaces to use with `mast` are, on the root machine:

* port `8008`, the `mast` controller itself
* port `8052`, Ansible AWX (`admin:password`)

```shell script
ansible-playbook -i mast-root,mast-1 00-lxd-machine.yml
ansible-playbook -i mast-root, -e ansible_connection=lxd 01-awx-docker.yml
# wait for setup to finish
curl "<mast-root-ip>:8008/get-new-machine-bootstrap?name=mast-1&parent=mast-root&connection_type=eth" > mast-1.zip
lxc file push mast-1.zip mast-1/root/
lxc exec mast-1 bash

# inside mast-1
cd /root/
unzip mast-1.zip
cd bundle-full-mast-1/
./01-device.sh
```

With physical machines, use `wifi` or `bluetooth` instead of `eth` if the machine supports it.

## Notes

_mast_ is in an alpha stage.
Security tries to even, but can't.
This is because default credentials and hardcoded passphrases are used everywhere; the design itself is sound.
